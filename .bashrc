# My additions
# ============

# Add python pip modules to path
export PATH=$PATH:$HOME/.local/bin

# Deno exports
export DENO_INSTALL="$HOME/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

# Vala Lint alias
alias vala-lint="io.elementary.vala-lint"
